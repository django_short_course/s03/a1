from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import GroceryItem
from django.template import loader
# Create your views here.
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

def index(request):
    groceryitem_list = GroceryItem.objects.all()
    # template = loader.get_template("index.html")
    context = {
        'groceryitem_list' : groceryitem_list,
        'user': request.user
    }
    return render(request, "index.html", context)

def groceryitem(request, groceryitem_id):
    res = f"You are viewing the details of {groceryitem_id}"
    return HttpResponse(res)

def register(request):
    users = User.objects.all()
    is_user_registered = False

    user = User()
    user.username = "janedoe"
    user.first_name = "jane"
    user.last_name = "doe"
    user.email = "janedoe@mail.com"
    user.set_password("jane1234")
    user.is_staff = False
    user.is_active = True

    for indiv_user in users:
        if indiv_user.username == user.username:
            is_user_registered = True
            break
    
    if is_user_registered == False:
        user.save()
    
    context = {
        'is_user_registered': is_user_registered,
        'first_name' : user.first_name,
        'last_name' : user.last_name
    }

    return render(request, "register.html", context)

def change_password(request):

    is_user_authenticated = False

    user = authenticate(username = "janedoe", password = "jane1234")

    if user is not None:
        authenticated_user = User.objects.get(username = 'janedoe')
        authenticated_user.set_password("jane12345")
        authenticated_user.save()

        is_user_authenticated = True
    
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "change_password.html", context)

def login_user(request):
    username = 'janedoe'
    password = 'jane12345'

    user = authenticate(username = username, password = password)

    if user is not None:
        login(request, user)
        return redirect('index')
    else:
        return render(request, 'login.html')

def logout_user(request):
    logout(request)
    return redirect('index')