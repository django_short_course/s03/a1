from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label = "username", max_length = 20)
    password = forms.CharField(label = "password", max_length = 20)

class AddTaskForm(forms.Form):
    task_name = forms.CharField(label = 'task_name', max_length = 50)
    description = forms.CharField(label = 'description', max_length = 200)