from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

# THe from keyword allows importing of necessary classes/ modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse
# Local imports
from .models import ToDoItem
# to use the template created:
from django.template import loader
# import the buil in user model
from django.contrib.auth.models import User
# import the authenticate function
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from .forms import LoginForm, AddTaskForm
from django.utils import timezone
def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
    # template = loader.get_template("index.html")
    context = {
        'todoitem_list': todoitem_list,
        'user': request.user
    }
    # output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    return render(request, "index.html", context)

def todoitem(request, todoitem_id):
    # response = f"You are viewing the details of {todoitem_id}"
    # return HttpResponse(response)
    todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

    return render(request, "todoitem.html", model_to_dict(todoitem))

# this function is responsible for registering on our application

def register(request):
    users = User.objects.all()
    is_user_registered = False

    user = User()
    user.username = "johndoe"
    user.first_name = "john"
    user.last_name = "doe"
    user.email = "john@mail.com"
    user.set_password("john1234")
    user.is_staff = False
    user.is_active = True

    for indiv_user in users:
        if indiv_user.username == user.username:
            is_user_registered = True
            break

    if is_user_registered == False:
        user.save()

    # to save our user

    context = {
        'is_user_registered' : is_user_registered,
        'first_name' : user.first_name,
        'last_name' : user.last_name
    }
    return render(request, "register.html", context)

def change_password(request):
    
    is_user_authenticated = False

    user = authenticate(username = "johndoe", password = "john1234")

    if user is not None:
        authenticated_user = User.objects.get(username = 'johndoe')
        authenticated_user.set_password("johndoe12345")
        authenticated_user.save()

        is_user_authenticated = True
    
    context = {
        "is_user_authenticated" : is_user_authenticated
    }

    return render(request, "change_password.html", context)

def login_user(request):
    context = {}

    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()
        else:
            username = form.cleaned_data["username"]
            password = form.cleaned_data['password']

            user = authenticate(username = username, password = password)

            if user is not None:
                context = {
                    'username' : username,
                    'password' : password
                }
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    'error' : True
                }
            
    return render(request, "login.html", context)


    # username = 'johndoe'
    # password = 'johndoe12345'

    # # is_user_authenticated = False
    
    # user = authenticate(username = username, password = password)

    

def logout_user(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):
    context = {}

    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

            if not duplicates:
                # create an object base on the Todoitem model and saves to the  record in the database
                ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id )

                return redirect("todolist:index")
            
            else:
                context = {
                    'error': True
                }

    return render(request, "add_task.html", context)
